import React from 'react';
import { Switch, Route, BrowserRouter } from 'react-router-dom'
import Login from './components/login/Login'
import ManagerView from './components/manager/ManagerView'
import EmployeeView from './components/employee/EmployeeView'
import AdminView from './components/admin/AdminView'

function Root() {

  return (
    <BrowserRouter>
      <Switch>
        <Route exact path="/" component={Login}/>
        <Route exact path="/manager" component={ManagerView}/>
        <Route exact path="/employee" component={EmployeeView}/>
        <Route exact path="/admin" component={AdminView}/>
      </Switch>
    </BrowserRouter>
  )
}

export default Root