import React from 'react'
import axios from '../../utils/axios'
import Button from '@material-ui/core/Button';
import CssBaseline from '@material-ui/core/CssBaseline';
import TextField from '@material-ui/core/TextField';
import Grid from '@material-ui/core/Grid';
import Typography from '@material-ui/core/Typography';
import Container from '@material-ui/core/Container';
import CONSTANTS from '../../utils/constants'
import { StatusCodes } from 'http-status-codes'

class Login extends React.Component{

  state={
    email: '',
    password: ''
  }

  onChange = event =>{
    this.setState({[event.target.name] : event.target.value})
  }
  
  handleLogin = () => {
    let user = {user: { email: this.state.email, password: this.state.password }}
    axios.post('/auth', user).then(resp => {
      console.log(resp.data.foundUser)
      if(resp.data.foundUser)
        if(resp.data.foundUser.role === 'manager')
          this.props.history.push('/manager')
        else if(resp.data.foundUser.role === 'employee')
          this.props.history.push('/employee')
        else this.props.history.push('/admin')
      else if(resp.data.status === CONSTANTS.STATUS.INCORECT_EMAIL)
        alert('Incorrect email')
      else alert('Incorrect password')
    })
  }
    render(){
        return(
        <Container component="main" maxWidth="xs" style={{marginTop: '30px'}}>
          <CssBaseline />
          <div>
            <Typography component="h1" variant="h5">
            {CONSTANTS.LOGIN}
            </Typography>
            <form noValidate>
              <TextField  
                variant="outlined"
                margin="normal"
                required
                fullWidth
                id="email"
                label="Adresa de email"
                name="email"
                autoComplete="email"
                autoFocus
                onChange={ this.onChange }
              />
              <TextField
                variant="outlined"
                margin="normal"
                required
                fullWidth
                name="password"
                label="Parola"
                type="password"
                id="password"
                autoComplete="current-password"
                onChange = { this.onChange }
              />
              <Button color="primary" variant="contained" fullWidth onClick={ this.handleLogin }>{CONSTANTS.LOGIN}</Button>
              
              <Grid container>
                <Grid item>
                </Grid>
              </Grid>
            </form>
          </div>
        </Container>
        )
    }
}

export default Login