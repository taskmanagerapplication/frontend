import React from 'react'
import Button from '@material-ui/core/Button';
import CssBaseline from '@material-ui/core/CssBaseline';
import TextField from '@material-ui/core/TextField';
import Grid from '@material-ui/core/Grid';
import Typography from '@material-ui/core/Typography';
import Container from '@material-ui/core/Container';
import axios from '../../utils/axios'
import Header from '../common/Header'
import validator from 'validator'
import { toast } from 'react-toastify'
import 'react-toastify/dist/ReactToastify.css'
import InputLabel from '@material-ui/core/InputLabel';
import FormControl from '@material-ui/core/FormControl';
import Select from '@material-ui/core/Select';
toast.configure()

class Register extends React.Component {

    state = {
        name : "",
        surname : "",
        email : "",
        phone : "",
        password: "",
        role: ""
        
    }

    handleRegister = () => {

        let surnameIsValid = validator.isAlpha(this.state.surname)
        let nameIsValid = validator.isAlpha(this.state.name)
        let emailIsValid = validator.isEmail(this.state.email)
        let phoneIsValid = validator.isNumeric(this.state.phone)
        let passwordIsValid = !validator.isEmpty(this.state.password)
        let roleIsValid = !validator.isEmpty(this.state.role)

        if(surnameIsValid && nameIsValid && emailIsValid && phoneIsValid && passwordIsValid && roleIsValid)
        {
            let user = {user : this.state}
            axios.post('/users', user).then(response =>{
                console.log(response)
            }, this.setState({
                name : "",
                surname : "",
                email : "",
                phone : "",
                password: "",
                role: ""
            }))
            toast.success('You have successfully enrolled the user')
        }
        else toast.error('Wrong credentials')
    }

    onChange = event =>{
        this.setState({ [event.target.name] : event.target.value })
    }

    onLogOut = () => {
        this.props.logout()
        this.props.history.push('/')
    }

    render() {
        return (
            <div>
                 <Header/>
                <Container component="main" maxWidth="xs">
                    <CssBaseline />
                <div style={{marginTop: '5%'}}>
                    <Typography component="h1" variant="h5">
                        Enroll
                    </Typography>

                    <form noValidate>
                        <TextField
                            variant="outlined"
                            margin="normal"
                            required
                            fullWidth
                            id="name"
                            label="Surname"
                            name="surname"
                            autoComplete="surname"
                            autoFocus
                            onChange={ this.onChange }
                            value={this.state.surname}
                        />
                        <TextField
                            variant="outlined"
                            margin="normal"
                            required
                            fullWidth
                            name="name"
                            label="Name"
                            type="name"
                            id="name"
                            autoComplete="name"
                            onChange={ this.onChange }
                            value={this.state.name}
                        />
                        <TextField
                            variant="outlined"
                            margin="normal"
                            required
                            fullWidth
                            id="email"
                            label="Email"
                            name="email"
                            autoComplete="email"
                            onChange={ this.onChange }
                            value={this.state.email}
                        />
                        <TextField
                            variant="outlined"
                            margin="normal"
                            required
                            fullWidth
                            name="phone"
                            label="Phone"
                            id="phone"
                            autoComplete="phone"
                            onChange={ this.onChange }
                            value={this.state.phone}
                        />
                        <TextField
                            variant="outlined"
                            margin="normal"
                            required
                            fullWidth
                            name="password"
                            label="Password"
                            type="password"
                            id="password"
                            autoComplete="current-password"
                            onChange={ this.onChange }
                            value={this.state.password}
                        />

                        <FormControl fullWidth variant="outlined" required margin="normal">
                                <InputLabel htmlFor="outlined-age-native-simple">Rol</InputLabel>
                                <Select
                                native
                                onChange={this.onChange}
                                name="role"
                                value={this.state.role}
                                >
                                <option aria-label="None" value="" />
                                <option value={"admin"}>Admin</option>
                                <option value={"manager"}>Manager</option>
                                <option value={"employee"}>Employee</option>
                                </Select>
                        </FormControl>

                        <Button variant="contained" color="primary" fullWidth onClick={ this.handleRegister }> Enroll </Button>

                        <Grid container>
                            <Grid item>
                            </Grid>
                        </Grid>
                    </form>
                </div>
            </Container>
            </div>
          )
    }
}

export default Register