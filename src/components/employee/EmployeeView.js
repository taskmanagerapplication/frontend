import React from 'react'
import Header from '../common/Header'
import List from '../common/List'

class EmployeeView extends React.Component{
    state={
        render: false
    }
    setRender = () =>{
        this.setState({render: !this.state.render})
    }
    render(){
        return(
            <div>
                <Header/>
                <div style={{display: 'flex', justifyContent: 'space-evenly', marginTop: '5%'}}>
                    <List status='in-progress' render={this.state.render} name='In-progress tasks'/>
                    <List status='new' setRender={this.setRender} name='New tasks'/>
                    
                </div>
            </div>
        )
    }
}

export default EmployeeView