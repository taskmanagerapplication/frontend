import React from 'react'
import axios from '../../utils/axios'
import { DataGrid, gridColumnLookupSelector } from '@material-ui/data-grid'
import TextField from '@material-ui/core/TextField'
import Button from '@material-ui/core/Button'
import Header from '../common/Header'
import List from '../common/List'

const columns = [
    { field: 'id', headerName: 'Nr', width: 70 },
    { field: 'lastName', headerName: 'First name', width: 120 },
    { field: 'firstName', headerName: 'Last name', width: 120 },
    { field: 'email', headerName: 'Email', width: 165},
    { field: 'phone', headerName: 'Phone', width: 120}
  ];
  
class ManagerView extends React.Component{

    state = {
        employees: [],
        rows: [],
        render: false,
        searchBar: ''
    }

    componentDidMount = () => {
        axios.get('/users').then(resp => {
            console.log(resp.data)
            this.setState({employees: resp.data}, this.setEmployees)
        })
    }

    setEmployees = () => {
        const rows = []
        this.state.employees.map((employee, index) => {
            rows[index] = { id: index+1, lastName: employee.surname, firstName: employee.name, email: employee.email, phone: employee.phone}
         })
         this.setState({rows: rows})
    }

    setRender = () => {
        this.setState({render: !this.state.render})
        
    }

    onChange = (event) => {
        this.setState({[event.target.name] : event.target.value})
    }

    onAddClick = () =>{
        console.log("HEHEHEH")
        let task = { name: this.state.searchBar, status: 'new'}
        axios.post('/tasks', {task}).then(resp => {
            console.log(resp)
        }, this.setState({searchBar: ''}))
    }

    render(){
        console.log(this.state.searchBar)
        return(
            <div>
                <Header/>
                <div style={{display: 'flex', justifyContent:'space-evenly', marginTop: '2%'}}>
                    <div style={{ height: 400, width: 600 }}>
                        <div style={{display: 'flex'}}>
                            <TextField label="Add new task" variant="filled" style={{marginBottom: '5%'}} fullWidth value={this.state.searchBar} name='searchBar' size='small' onChange={this.onChange}/>
                            <div style={{justifyContent: 'flex-end', margin:'5px'}}>
                                <Button onClick={() => this.onAddClick()} variant='contained' color="primary" size='large' > ADD </Button>
                            </div>
                        </div>
                        <DataGrid rows={this.state.rows} columns={columns} pageSize={5}/>
                    </div>
                    <List status={'verification'} setRender={this.setRender} name='In verification tasks'/>
                    <List status={'closed'} render={this.state.render} name='Closed tasks'/>
                </div> 
            </div>
        )
    }
}

export default ManagerView