import React, { useEffect } from 'react';
import { makeStyles } from '@material-ui/core/styles';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import ListItemSecondaryAction from '@material-ui/core/ListItemSecondaryAction';
import ListItemText from '@material-ui/core/ListItemText';
import Checkbox from '@material-ui/core/Checkbox';
import Button from '@material-ui/core/Button';
import ListSubheader from '@material-ui/core/ListSubheader'
import axios from '../../utils/axios'

const useStyles = makeStyles((theme) => ({
  root: {
    width: '100%',
    maxWidth: 360,
    backgroundColor: '#ccffff',
    color: '#3333cc'
  },
  listHeader: {
    backgroundColor: '#99ff66',
    color: 'black'
  }
}));

export default function CheckboxList(props) {
  const classes = useStyles();
  const [tasks, setTasks] = React.useState([])

  useEffect(() => {
    getTasks()
  },[props.render])

  const getTasks = () => {
    axios.get(`/tasks?status=${props.status}`).then(resp => {
      console.log(resp)
      setTasks(resp.data)
      
    })
  }

  const onActionClick = (action, task) =>{
      console.log(action)
      task.status = action
      axios.put(`/tasks/${task._id}`, {task}).then(resp => {
        if(action!='verification')
          props.setRender()
        getTasks()
      })
  }

  return (
    <List className={classes.root} subheader={


      <ListSubheader className={classes.listHeader} component="div" id="nested-list-subheader">
        {props.name}
      </ListSubheader>
    }>
      {tasks.map((task, value) => {
        const labelId = `checkbox-list-label-${value}`;
        

        return (
          
          <div>
            {props.status === 'verification' ?
                <ListItem key={value} role={undefined} dense button >
                  <ListItemText primary={task.name} />
                  <Button onClick={() => onActionClick('in-progress', task)} variant="contained" color="primary" size='small'>
                      REFACTOR
                  </Button>
                  <div style={{marginLeft:'10px'}}>
                  <Button onClick={() => onActionClick('closed', task)} variant='contained' color="default" size='small'>
                      CLOSE
                  </Button>
                  </div>
                </ListItem>
                :
              props.status === 'closed' ?
                <ListItem key={value} role={undefined} dense button >
                  <ListItemText primary={task.name} />
                </ListItem> 
                :
              props.status === 'in-progress' ?
                <ListItem key={value} role={undefined} dense button >
                  <ListItemText primary={task.name} />
                  <Button onClick={() => onActionClick('verification', task)} variant='contained' color="default" size='small'>
                      VERIFY
                  </Button>
                </ListItem> 
                :
                <ListItem key={value} role={undefined} dense button >
                  <ListItemText primary={task.name} />
                  <Button onClick={() => onActionClick('in-progress', task)} variant='contained' color="default" size='small'>
                      SELECT
                  </Button>
                </ListItem>

           }
          </div>
        );
      })}
    </List>
    
  );
}
